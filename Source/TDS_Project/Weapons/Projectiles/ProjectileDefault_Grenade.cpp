// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "../../Interface/TDS_IGameActor.h"
#include "DrawDebugHelpers.h"


int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		TimerExplose(DeltaTime);
	}
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose_OnServer();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose_OnServer();

	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}


void AProjectileDefault_Grenade::Explose_OnServer_Implementation()
{
	if (DebugExplodeShow)
	{
		//DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		//DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
		SpawnExplodeDebugSphere_Multicast(GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, FColor::Green);
		SpawnExplodeDebugSphere_Multicast(GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, FColor::Red);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		SpawnExplodeFx_Multicast(ProjectileSetting.ExplodeFX);
	}
	if (ProjectileSetting.ExplodeSound)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
		SpawnExplodeSound_Multicast(ProjectileSetting.ExplodeSound);
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}

void AProjectileDefault_Grenade::SpawnExplodeDebugSphere_Multicast_Implementation(FVector Location, float SphereRadius,	FColor Color)
{
	DrawDebugSphere(GetWorld(), Location, SphereRadius, 12, Color, false, 12.0f);
}

void AProjectileDefault_Grenade::SpawnExplodeSound_Multicast_Implementation(USoundBase* ExplodeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
}

void AProjectileDefault_Grenade::SpawnExplodeFx_Multicast_Implementation(UParticleSystem* FX)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}
