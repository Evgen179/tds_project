// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTDS_Project, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTDS_Project_Net, Log, All);